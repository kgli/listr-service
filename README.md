# Listr Service
This is a Spring Boot-based microservice for aggregating RSS feeds. This is especially useful for tracking ads from multiple classifieds sites.

## Installation
1. `mvn clean install`
2. `mvn spring-boot:run`

## Usage
1. Make POST request to "/v1/feeds/get" with the following request body:
```
{
	"feeds": [
		"[INSERT RSS FEED URL HERE]",
		"[INSERT RSS FEED URL HERE]",
		...
	]
}
```

## Future Features
* Add support for Swagger UI when there is better support for the functional route handlers (see https://github.com/springfox/springfox/issues/1773)
