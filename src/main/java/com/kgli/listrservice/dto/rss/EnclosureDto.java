package com.kgli.listrservice.dto.rss;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EnclosureDto {

    @JacksonXmlProperty(isAttribute = true)
    private String resource;

    @JacksonXmlProperty(isAttribute = true)
    private String url;
}