package com.kgli.listrservice.dto.rss;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ItemDto {

    @JacksonXmlProperty
    private String title;

    @JacksonXmlProperty
    private String description;

    @JacksonXmlProperty
    private String link;

    @JacksonXmlProperty
    private EnclosureDto enclosure;
}
