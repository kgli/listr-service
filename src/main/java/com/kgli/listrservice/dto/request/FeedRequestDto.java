package com.kgli.listrservice.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class FeedRequestDto {

    private List<String> feeds;
}
