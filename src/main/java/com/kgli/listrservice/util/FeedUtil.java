package com.kgli.listrservice.util;

import com.kgli.listrservice.model.Feed;
import com.kgli.listrservice.model.Status;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.reactive.function.client.WebClientResponseException;

@Slf4j
public class FeedUtil {

    private FeedUtil() {
    }

    public static Feed buildErrorFeed(String url, Throwable error) {
        log.debug("-- Logging the error for the request '{}': '{}'", url, error.getMessage());

        Status.Code statusCode = getStatusCode(error);

        final Status status = Status.builder()
                .code(statusCode)
                .details(statusCode.getFormattedErrorMessage(error))
                .build();

        return Feed.builder()
                .status(status)
                .build();
    }

    private static Status.Code getStatusCode(Throwable error) {
        if (error instanceof WebClientResponseException) {
            return Status.Code.REQUEST_ERROR;
        }
        else {
            return Status.Code.PARSE_ERROR;
        }
    }
}
