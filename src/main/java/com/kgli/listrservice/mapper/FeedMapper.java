package com.kgli.listrservice.mapper;

import com.kgli.listrservice.dto.rss.ChannelDto;
import com.kgli.listrservice.dto.rss.FeedDto;
import com.kgli.listrservice.model.Feed;
import com.kgli.listrservice.model.Status;
import org.mapstruct.*;

import java.util.Optional;

@Mapper(componentModel = "spring", uses = ItemMapper.class)
public interface FeedMapper {

    Status OK_STATUS = Status.builder()
            .code(Status.Code.OK)
            .build();

    @BeforeMapping
    default void moveChannelItemsToRoot(FeedDto feedDto) {
        if (feedDto != null) {
            Optional.of(feedDto)
                    .map(FeedDto::getChannel)
                    .map(ChannelDto::getItems)
                    .ifPresent(feedDto::setItems);
        }
    }

    @Mapping(target = "status", expression = "java(OK_STATUS)")
    Feed convertFeedDtoToFeed(FeedDto feedDto);
}