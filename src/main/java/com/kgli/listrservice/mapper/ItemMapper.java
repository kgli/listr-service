package com.kgli.listrservice.mapper;

import com.kgli.listrservice.dto.rss.ItemDto;
import com.kgli.listrservice.model.Item;
import org.apache.commons.text.StringEscapeUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.Optional;

@Mapper(componentModel = "spring")
public interface ItemMapper {

    Item itemDtoToItem(final ItemDto itemDto);

    @AfterMapping
    default void afterMapping(ItemDto itemDto, @MappingTarget Item item) {
        mapImageUrlToItem(itemDto, item);
        mapUnescapedTitleToItem(itemDto, item);
        mapUnescapedDescriptionToItem(itemDto, item);
    }

    default void mapImageUrlToItem(ItemDto itemDto, Item item) {
        Optional.of(itemDto)
                .map(ItemDto::getEnclosure)
                .map(enclosure -> enclosure.getUrl() == null ? enclosure.getResource() : enclosure.getUrl())
                .ifPresent(item::setImageUrl);
    }

    default void mapUnescapedTitleToItem(ItemDto itemDto, Item item) {
        Optional.of(itemDto)
                .map(ItemDto::getTitle)
                .map(StringEscapeUtils::unescapeHtml3)
                .ifPresent(item::setTitle);

    }

    default void mapUnescapedDescriptionToItem(ItemDto itemDto, Item item) {
        Optional.of(itemDto)
                .map(ItemDto::getDescription)
                .map(StringEscapeUtils::unescapeHtml3)
                .ifPresent(item::setDescription);
    }
}
