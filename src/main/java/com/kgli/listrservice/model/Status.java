package com.kgli.listrservice.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class Status {

    public enum Code {
        OK(""),
        REQUEST_ERROR("Error requesting for the RSS feed: '%s'"),
        PARSE_ERROR("Error parsing the RSS response: '%s'");

        private String baseMessage;

        Code(String baseMessage){
            this.baseMessage = baseMessage;
        }

        public String getFormattedErrorMessage(Throwable throwable) {
            return String.format(baseMessage, throwable.getMessage());
        }
    }

    private Code code;

    private String details;
}
