package com.kgli.listrservice.model;

import lombok.*;

import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class Feed {

    private Status status;

    private List<Item> items;

    public Feed(List<Item> items) {
        this.items = items;
    }
}

