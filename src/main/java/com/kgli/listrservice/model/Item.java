package com.kgli.listrservice.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Item {

    private String title;

    private String description;

    private String link;

    private String imageUrl;
}
