package com.kgli.listrservice.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.kgli.listrservice.dto.request.FeedRequestDto;
import com.kgli.listrservice.dto.rss.FeedDto;
import com.kgli.listrservice.mapper.FeedMapper;
import com.kgli.listrservice.model.Feed;
import com.kgli.listrservice.util.FeedUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.server.ServerRequest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class RssService {

    private final WebClient webClient;

    private final FeedMapper feedMapper;

    private final XmlMapper xmlMapper;

    @Autowired
    public RssService(WebClient.Builder webClientBuilder, FeedMapper feedMapper) {
        this.webClient = webClientBuilder.build();
        this.feedMapper = feedMapper;
        this.xmlMapper = new XmlMapper();
        this.xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public Flux<Feed> getFeeds(ServerRequest request) {
        log.debug("-- Processing request body: {}", request);

        return request.bodyToMono(FeedRequestDto.class)
            .map(FeedRequestDto::getFeeds)
            .map(this::executeFeedRequests)
            .flatMapMany(Flux::merge);
    }

    private List<Mono<Feed>> executeFeedRequests(List<String> feedUrls) {
        log.debug("-- Executing feed requests: {}", feedUrls);

        List<Mono<Feed>> feedRequests = new ArrayList<>();

        for (String feedUrl :feedUrls) {
            Mono<Feed> feed = getFeedResponseAsString(feedUrl)
                    .map(responseString -> convertResponseStringToFeedDto(feedUrl, responseString))
                    .map(feedMapper::convertFeedDtoToFeed)
                    .onErrorResume(error -> Mono.just(FeedUtil.buildErrorFeed(feedUrl, error)));
            feedRequests.add(feed);
        }

        return feedRequests;
    }

    private Mono<String> getFeedResponseAsString(String url) {
        log.debug("-- Retrieve RSS response from '{}' as string", url);

        return webClient
                .get()
                .uri(url)
                .retrieve()
                .bodyToMono(String.class); // Ideally, we should be able to use bodyToMono(FeedDto.class)
    }

    private FeedDto convertResponseStringToFeedDto(String url, String contentString) throws RuntimeException {
        log.debug("-- Converting String response from '{}' to dto", url);

        try {
            return xmlMapper.readValue(contentString, FeedDto.class);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }
}
