package com.kgli.listrservice.routing;

import com.kgli.listrservice.model.Feed;
import com.kgli.listrservice.service.RssService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Component
public class FeedHandler {

    @Autowired
    private RssService rssService;

    public Mono<ServerResponse> getFeed(ServerRequest request) {
        return ServerResponse.ok()
                .body(rssService.getFeeds(request), Feed.class);
    }

    public Mono<ServerResponse> getUnifiedFeed(ServerRequest request) {
        return ServerResponse.ok()
                .body(rssService.getFeeds(request), Feed.class);
    }
}
