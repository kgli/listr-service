package com.kgli.listrservice.routing;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.*;

@Configuration
public class FeedRouter {

    @Bean
    public RouterFunction<ServerResponse> route(FeedHandler feedHandler) {
        return RouterFunctions
                .route(RequestPredicates.POST("/v1/feed/get")
                        .and(RequestPredicates.contentType(MediaType.APPLICATION_JSON)), feedHandler::getFeed);
    }
}
