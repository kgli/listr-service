package com.kgli.listrservice.service;

import com.kgli.listrservice.dto.request.FeedRequestDto;
import com.kgli.listrservice.mapper.FeedMapper;
import com.kgli.listrservice.model.Feed;
import com.kgli.listrservice.model.Status;
import com.squareup.okhttp.mockwebserver.MockResponse;
import com.squareup.okhttp.mockwebserver.MockWebServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.reactive.function.server.MockServerRequest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.server.ServerRequest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_RSS_XML_VALUE;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RssServiceTest {

    @Autowired
    private FeedMapper feedMapper;

    @Mock
    private WebClient.Builder webClientBuilder;

    @MockBean
    private RssService rssService;

    private MockWebServer mockWebServer;

    @Before
    public void setUp() {
        mockWebServer = new MockWebServer();
        when(webClientBuilder.build()).thenReturn(WebClient.create(mockWebServer.url("/").toString()));
        rssService = new RssService(webClientBuilder, feedMapper);
    }

    @After
    public void cleanUp() throws IOException {
        mockWebServer.shutdown();
    }

    @Test
    public void requestMultipleFeedsSuccess() throws IOException {
        List<String> feedUrls = Arrays.asList(
                "/rss-srp-cars-trucks/greater-vancouver-area/miata/k0c174l80003",
                "/search/sss?query=miata&sort=rel"
        );

        String mockKijijiServerResponse = readMockResponseFileAsString("kj_response.xml");
        String mockCraigslistServerResponse = readMockResponseFileAsString("cl_response.xml");

        mockWebServer.enqueue(
                new MockResponse()
                        .setResponseCode(200)
                        .setHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_RSS_XML_VALUE)
                        .setBody(mockKijijiServerResponse)
        );
        mockWebServer.enqueue(
                new MockResponse()
                        .setResponseCode(200)
                        .setHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_RSS_XML_VALUE)
                        .setBody(mockCraigslistServerResponse)
        );

        List<Feed> feeds = executeFeedRequests(feedUrls)
                .collectList()
                .block();

        assertEquals(2, feeds.size());
        feeds.forEach(responseFeed -> {
            assertEquals(Status.Code.OK, responseFeed.getStatus().getCode());
            assertEquals(2, responseFeed.getItems().size());
        });
    }

    @Test
    public void requestSingleFeedNotFound404() {
        List<String> requestUrls = Collections.singletonList(
                "/search/sss?query=miata&sort=rel"
        );

        mockWebServer.enqueue(
                new MockResponse()
                        .setResponseCode(404)
                        .setHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_RSS_XML_VALUE)
        );

        List<Feed> responseFeeds = executeFeedRequests(requestUrls)
                .collectList()
                .block();

        assertEquals(1, responseFeeds.size());
        responseFeeds.forEach(responseFeed -> assertEquals(Status.Code.REQUEST_ERROR, responseFeed.getStatus().getCode()));
    }

    @Test
    public void requestSingleFeedInternalServerError500() {
        List<String> requestUrls = Collections.singletonList(
                "/search/sss?query=miata&sort=rel"
        );

        mockWebServer.enqueue(
                new MockResponse()
                        .setResponseCode(500)
                        .setHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_RSS_XML_VALUE)
        );

        List<Feed> responseFeeds = executeFeedRequests(requestUrls)
                .collectList()
                .block();

        assertEquals(1, responseFeeds.size());
        responseFeeds.forEach(responseFeed -> assertEquals(Status.Code.REQUEST_ERROR, responseFeed.getStatus().getCode()));
    }

    @Test
    public void requestSingleFeedParsingError() {
        List<String> requestUrls = Collections.singletonList(
                "/search/sss?query=miata&sort=rel"
        );

        mockWebServer.enqueue(
                new MockResponse()
                        .setResponseCode(200)
                        .setHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_RSS_XML_VALUE)
                        .setBody("12345")
        );

        List<Feed> responseFeeds = executeFeedRequests(requestUrls)
                .collectList()
                .block();

        assertEquals(1, responseFeeds.size());
        responseFeeds.forEach(responseFeed -> assertEquals(Status.Code.PARSE_ERROR, responseFeed.getStatus().getCode()));
    }

    @Test
    public void requestFeedPartialSuccess() throws IOException {
        String mockCraigslistServerResponse = readMockResponseFileAsString("cl_response.xml");

        List<String> requestUrls = Arrays.asList(
                "/search/sss?query=miata&sort=rel",
                "/search/sss?query=ilx&sort=rel",
                "/search/sss?query=ptcruiser&sort=rel"
        );

        mockWebServer.enqueue(
                new MockResponse()
                        .setResponseCode(400)
                        .setHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_RSS_XML_VALUE)
        );
        mockWebServer.enqueue(
                new MockResponse()
                        .setResponseCode(200)
                        .setHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_RSS_XML_VALUE)
                        .setBody("12345")
        );
        mockWebServer.enqueue(
                new MockResponse()
                        .setResponseCode(200)
                        .setHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_RSS_XML_VALUE)
                        .setBody(mockCraigslistServerResponse)
        );

        List<Feed> responseFeeds = executeFeedRequests(requestUrls)
                .collectList()
                .block();

        Stream<Status.Code> responseCodes = responseFeeds.stream()
                .map(responseFeed -> responseFeed.getStatus().getCode())
                .distinct();
        assertEquals(3, responseCodes.count());


        Optional<Feed> validFeedOpt = responseFeeds.stream()
                .filter(feed -> feed.getStatus().getCode().equals(Status.Code.OK))
                .findFirst();
        assertTrue(validFeedOpt.isPresent());
        assertEquals(2, validFeedOpt.get().getItems().size());
    }

    private Flux<Feed> executeFeedRequests(List<String> requestUrls) {
        FeedRequestDto requestDto = new FeedRequestDto();
        requestDto.setFeeds(requestUrls);

        ServerRequest request = MockServerRequest.builder().body(Mono.just(requestDto));
        return rssService.getFeeds(request);
    }

    private String readMockResponseFileAsString(String fileName) throws IOException {
        return new String(Files.readAllBytes(Paths.get("src/test/resources/" + fileName)));
    }
}
