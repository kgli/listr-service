package com.kgli.listrservice.mapper;

import com.kgli.listrservice.dto.rss.ChannelDto;
import com.kgli.listrservice.dto.rss.FeedDto;
import com.kgli.listrservice.dto.rss.ItemDto;
import com.kgli.listrservice.model.Feed;
import com.kgli.listrservice.model.Item;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FeedMapperTest {

    @Autowired
    private FeedMapper feedMapper;

    @Autowired
    private ItemMapper itemMapper;

    @Test
    public void convertNullFeedDtoToFeedSuccess() {
        Feed outputFeed = feedMapper.convertFeedDtoToFeed(null);
        assertNull(outputFeed);
    }

    @Test
    public void convertEmptyFeedDtoToFeedSuccess() {
        FeedDto inputDto = FeedDto.builder().build();
        Feed outputFeed = feedMapper.convertFeedDtoToFeed(inputDto);
        assertNotNull(outputFeed);
    }

    @Test
    public void convertFeedDtoWithRootItemsToFeedSuccess() {
        List<ItemDto> inputItemDtos = buildItemDtoList(2);

        FeedDto inputFeedDto = FeedDto.builder()
                .items(inputItemDtos)
                .build();

        Feed outputFeed = feedMapper.convertFeedDtoToFeed(inputFeedDto);
        List<Item> outputItems = outputFeed.getItems();

        assertEquals(inputItemDtos.size(), outputItems.size());
        validateFeedItems(inputItemDtos, outputItems);
    }

    @Test
    public void convertFeedDtoWithChannelItemsToFeedSuccess() {
        List<ItemDto> inputItemDtos = buildItemDtoList(3);

        ChannelDto inputChannelDto = ChannelDto.builder()
                .items(inputItemDtos)
                .build();

        FeedDto inputFeedDto = FeedDto.builder()
                .channel(inputChannelDto)
                .items(new ArrayList<>())
                .build();

        Feed outputFeed = feedMapper.convertFeedDtoToFeed(inputFeedDto);
        List<Item> outputItems = outputFeed.getItems();

        assertEquals(inputItemDtos.size(), outputItems.size());
        validateFeedItems(inputItemDtos, outputItems);
    }

    private void validateFeedItems(List<ItemDto> inputItemDtos, List<Item> outputItems) {
        for (int i = 0; i < outputItems.size(); i++) {
            ItemDto inputItem = inputItemDtos.get(i);
            Item outputItem = outputItems.get(i);

            assertEquals(inputItem.getTitle(), outputItem.getTitle());
            assertEquals(inputItem.getDescription(), outputItem.getDescription());
            assertEquals(inputItem.getLink(), outputItem.getLink());
        }
    }

    private List<ItemDto> buildItemDtoList(final int numItems) {
        List<ItemDto> feedItems = new ArrayList<>();

        for (int i = 0; i < numItems; i++) {
            feedItems.add(ItemDto.builder()
                    .title("Title " + i)
                    .description("Description: " + i)
                    .link("Link: " + i)
                    .build()
            );
        }

        return feedItems;
    }
}
