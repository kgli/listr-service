package com.kgli.listrservice.mapper;

import com.kgli.listrservice.dto.rss.EnclosureDto;
import com.kgli.listrservice.dto.rss.ItemDto;
import com.kgli.listrservice.model.Item;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ItemMapperTest {

    private ItemMapper itemMapper = new ItemMapperImpl();

    @Test
    public void convertNullItemDtoSuccess() {
        Item outputItem = itemMapper.itemDtoToItem(null);
        assertNull(outputItem);
    }

    @Test
    public void convertItemDtoWithEnclosureUrlToItemSuccess() {
        EnclosureDto inputEnclosureDto = EnclosureDto.builder()
                .url("Image url 1")
                .build();

        ItemDto inputItemDto = ItemDto.builder()
                .enclosure(inputEnclosureDto)
                .build();

        Item outputItem = itemMapper.itemDtoToItem(inputItemDto);

        assertEquals(inputEnclosureDto.getUrl(), outputItem.getImageUrl());
    }

    @Test
    public void convertItemDtoWithEnclosureResourceToItemSuccess() {
        EnclosureDto inputEnclosureDto = EnclosureDto.builder()
                .resource("Image url 2")
                .build();

        ItemDto inputItemDto = ItemDto.builder()
                .enclosure(inputEnclosureDto)
                .build();

        Item outputItem = itemMapper.itemDtoToItem(inputItemDto);

        assertEquals(inputEnclosureDto.getResource(), outputItem.getImageUrl());
    }

    @Test
    public void convertItemDtoWithUnescapedTitleToItemSuccess() {
        ItemDto inputItemDto = ItemDto.builder()
                .title("Thingy &#x0024;100")
                .build();

        Item outputItem = itemMapper.itemDtoToItem(inputItemDto);

        assertEquals("Thingy $100", outputItem.getTitle());
    }
}
